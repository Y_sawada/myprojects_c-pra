#pragma once
#include <vector>

class Actor
{
public:
	// Actorの状態
	enum State
	{
		EActive,
		EPaused,
		EDead
	};
	// コンストラクタ
	Actor(class Game* game);

	// コンポーネント追加
	void AddComponent(class Component* component);

	class Game* GetGame() { return m_Game; }

private:
	State m_State;

	// コンポーネントを持つ
	std::vector<class Component*> m_Componnets;
	class Game* m_Game;
};