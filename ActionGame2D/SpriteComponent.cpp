#include "SpriteComponent.h"
#include "Actor.h"
#include "Game.h"

SpriteComponent::SpriteComponent(Actor* owner, int drawOrder)
	:Component(owner)
	,m_DrawOrder(drawOrder)
	,m_TexWidth(0)
	,m_TexHeight(0)
{
	m_Owner->GetGame()->AddSprite(this);
}