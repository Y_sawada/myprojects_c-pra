#include "Game.h"

int main()
{
	Game game;
	bool success = game.Initialize();
	if (success)
	{
		game.RunGame();
	}
	else
	{
		printf("Game Initialize Error");
		game.Close();
		return -1;
	}
	game.Close();
	return 0;
}