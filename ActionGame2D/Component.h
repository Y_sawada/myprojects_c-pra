#pragma once

class Component
{
public:
	// コンストラクタ
	Component(class Actor* owner, int updateOrder = 100);

	// ゲッター
	int GetUpdateOrder() const { return m_UpdateOrder; }

protected:
	// このコンポーネントの所有者
	class Actor* m_Owner;

	// コンポーネントの更新順序
	int m_UpdateOrder;
};