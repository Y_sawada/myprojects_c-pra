#pragma once
#include <gl/glew.h>       // 拡張ライブラリ
#include <GLFW/glfw3.h>    // ウィンドウ作成
#include <glm/glm.hpp>     // 数学ライブラリ
#include <stdio.h>
#include <vector>

class Game
{
public:
	Game();
	bool Initialize();
	void RunGame();
	void Close();

	// アクター
	void AddActor(class Actor* actor);
	void RemoveActor(class Actor* actor);

	// スプライト
	void AddSprite(class SpriteComponent* sprite);

private:

	bool InitGLFW();
	bool InitGLEW();

	GLFWwindow* GLwindow;
	GLuint VertexArrayID;

	const int HEIGHT = 480;
	const int WIDTH = 640;
	const char* Title = "ActionGame_2D";

	std::vector<class Actor*> m_Actors;
	std::vector<class Actor*> m_PendingActors;

	std::vector<class SpriteComponent*> m_Sprites;
	bool isRunning;

	// 現在Actorを更新しているか
	bool m_UpdatingActors;
};