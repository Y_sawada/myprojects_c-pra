#pragma once

#include "Component.h"

class SpriteComponent : public Component
{
public:
	// コンストラクタ
	SpriteComponent(class Actor* owner, int drawOrder = 100);

	int GetDrawOrder() const { return m_DrawOrder; }
protected:
	int m_DrawOrder;
	int m_TexWidth;
	int m_TexHeight;
};