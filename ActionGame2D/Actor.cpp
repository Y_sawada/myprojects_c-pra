#include "Actor.h"
#include "Game.h"
#include "Component.h"

// コンストラクタ
Actor::Actor(Game* game)
	:m_State(EActive)
	,m_Game(game)
{
	// Actorを追加
	m_Game->AddActor(this);
}

void Actor::AddComponent(Component* component)
{
	int myOrder = component->GetUpdateOrder();

	// m_Conponentsの最初の値
	auto iter = m_Componnets.begin();
	for (; iter != m_Componnets.end(); ++iter)
	{
		// 追加するコンポーネントの更新順序がイテレータの更新順序より小さかったらBreak
		if (myOrder < (*iter)->GetUpdateOrder())
		{
			break;
		}
	}

	// 上記で抜けたポインタの部分に今のコンポーネントを追加
	m_Componnets.insert(iter, component);
}