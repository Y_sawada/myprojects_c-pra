#include "Component.h"
#include "Actor.h"

Component::Component(class Actor* owner, int updateOrder)
	:m_Owner(owner)
	,m_UpdateOrder(updateOrder)
{
	m_Owner->AddComponent(this);
}