#include "Game.h"
#include "Actor.h"
#include "SpriteComponent.h"

Game::Game()
	:GLwindow(nullptr)
	,m_UpdatingActors(false)
{
}

// 初期化
bool Game::Initialize()
{
	// GLFWの初期化
	if (!InitGLFW()) return false;

	// GLEWの初期化
	if (!InitGLEW()) return false;

	// カラーバッファのクリア
	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);

	return true;
}

bool Game::InitGLFW()
{
	// GLFW初期化
	if (!glfwInit())
	{
		printf("GLFW Initialize Error");
		return false;
	}

	// ウィンドウ作成
	GLwindow = glfwCreateWindow(WIDTH, HEIGHT, Title, NULL, NULL);
	if (!GLwindow)
	{
		printf("Window Create Error");
		glfwTerminate();
		return false;
	}

	// コンテキスト生成
	glfwMakeContextCurrent(GLwindow);

	return true;
}

bool Game::InitGLEW()
{
	glewExperimental = true;
	if (glewInit() != GLEW_OK)
	{
		printf("GLEW Initalize Error");
		glfwTerminate();
		return false;
	}
	return true;
}

// 更新
void Game::RunGame()
{
	// 終了されるまでループ
	while (!glfwWindowShouldClose(GLwindow))
	{


		// OpenGLで三角形の描画



		glClear(GL_COLOR_BUFFER_BIT);

		// ダブルバッファのスワップ
		glfwSwapBuffers(GLwindow);
		glfwPollEvents();
	}
}

// 終了
void Game::Close()
{
	glfwTerminate();
}

void Game::AddActor(Actor* actor)
{
	// Actorを更新中なら
	if (m_UpdatingActors)
	{
		// 待機中に入れる
		m_PendingActors.emplace_back(actor);
	}
	// 更新中でなければActorを追加
	else
	{
		m_Actors.emplace_back(actor);
	}
}

void Game::RemoveActor(Actor* actor)
{

}

void Game::AddSprite(SpriteComponent* sprite)
{
	int myDarwOrder = sprite->GetUpdateOrder();
	auto iter = m_Sprites.begin();
	for (; iter != m_Sprites.end(); ++iter)
	{
		if (myDarwOrder < (*iter)->GetDrawOrder())
		{
			break;
		}
	}

	m_Sprites.insert(iter, sprite);
}